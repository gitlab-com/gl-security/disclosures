# eramba - Persistent XSS

eramba Community Edition c2.8.1 and Enterprise Edition before e2.19.3 are
vulnerable to persistent Cross Site Scripting via attachment filenames.

# Affected Products

* eramba Enterprise prior to e2.19.3
* eramba Community Edition up to and including c2.8.1. Note that the Community
  Edition receives patches once a year and therefore will remain vulnerable
  until the next patch cycle which is expected in November 2020.

# Disclosure Timeline

* 2020-08-31 initial report to eramba
* 2020-08-31 eramba team released a fix for Enterprise Edition and a description of the vulnerability
* 2020-09-03 CVE Number assigend
* 2020-09-04 Release of this advisory

# CVE Identifier

 * [CVE-2020-25104](https://nvd.nist.gov/vuln/detail/CVE-2020-25104)

# Details

Wihtin eramba it is possible to attach files to pretty much any object. By
using carfted filenames it is possible to exploit a persistent Cross Site
Scripting vulnerability in eramba.

The issue can be demonstrated by attaching file named 
`<img src=x onerror=alert('persistent_xss_in_eramba'')>.png` 
to e.g. the `admin` user (user ID 1)  within eramba.

The XSS payload will execute when visting `/widget/widget/story/User/1`
afterwards, as shown in the screenshot.

![xss](./erambaxss.png)

# External References

* [eramba's announcement](https://discussions.eramba.org/t/bug-security-vulnerabilities-not-serious/1650)

# Credits

Joern Schneeweisz - GitLab Security Research Team

