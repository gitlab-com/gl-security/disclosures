# Disclosures

This repository holds the advisories for security issues identified in third party products.

For an advisory [template](https://gitlab.com/gitlab-com/gl-security/disclosures/-/blob/template/template.md?ref_type=heads) see the `template` branch of this repository. 